# ChatRZO!

Bienvenue sur la documentation du projet **ChatRZO**.

## Introduction
Ce projet a pour but de développer une application de chat dans un terminal (un terminal pouvant communiquer avec un autre terminal par le biais d'un server intermédiaire).
Le projet a été découpé en 3 livrables intermédiaires :

- **PIPES** : Développer une application 'server' et une application 'client'. Le server doit être capable d'accepter plusieurs connexions et plusieurs clients à la fois. Les communications entre le server et les clients doivent impérativement utiliser les pipes (comme vu en cours). Pour ce premier livrable de début de projet, il n'est pas prévu de faire communiquer les clients entre eux à travers le server.

- **SOCKETS** : Développer une application 'server' et une application 'client'. Le server doit être capable d'accepter plusieurs connexions et plusieur clients à la fois. Les communications entre le server et les clients doivent impérativement utiliser les sockets (à comprendre par nous-mêmes). Pour ce second livrable, il est prévu de faire communiquer les clients entre eux à travers le server, même si celà doit se résumer à de simples actions limitées.
- **FEATURES** : Ce livrable à pour objectif de reprendre le précédent livrable 'SOCKET' et d'implémenter de nombreuses features, dont voici quelques exemples : message général, message privé, pseudonymes...


Dans la suite de ce document, on vous explique comment faire fonctionner notre projet afin que vous puissiez le tester sur votre propre machine !

Ce projet est développé de manière bénévole par Yann LEFEVRE, Nathan DESPRES, Jérémie LEVY et Thomas GILBERT dans le cadre de notre cours de Système d'Exploitation animé par Julien COZZON-BERGER.
Si vous détectez des erreurs ou des points d'amélioration, merci d'en faire part à un des membres de l'équipe afin que le nécessaire soit fait.

## Sommaire
- Récupération du projet
- Compilation et utilisation du livrable **PIPES**
- Compilation et utilisation du livrable **SOCKETS** (coming soon)

Les deux livrables sont indépendants l'un de l'autre (cf introduction) et sont donc compilés séparément.

## Récupération du projet
L'intégralité du code source se situe sur ce repository gitlab. Vous pouvez le récupérer de la manière dont vous souhaitez, mais nous vous conseillons de le cloner :
- HTTPS : `git clone https://gitlab.com/zeusxduff/chatRZO.git`
- SSH : `git clone git@gitlab.com:zeusxduff/chatRZO.git`

Si tout s'est bien passé, vous devriez obtenir l'arborescance suivante après exécution de la commande `tree` :
```
.
├── pipes/
│   ├── build/
│       └── dossier optionnel, peut importe ce que vous avez ici
│   ├── headers/
│       ├── client.h
│       ├── const.h
│       ├── server.h
│       └── utils.h
│   ├── makefile
│   └── src/
│       ├── client.c
│       ├── server.c
│       └── utils.c
├── sockets/
│   ├── build/
│       └── dossier optionnel, peut importe ce que vous avez ici
│   ├── headers/
│       ├── client.h
│       ├── const.h
│       ├── server.h
│       └── utils.h
│   ├── makefile
│   └── src/
│       ├── client.c
│       ├── server.c
│       └── utils.c
└── README.md
```

## Compilation du livrable PIPES

**1. Build du projet**
Ouvrez un terminal à la racine du projet : chatRZO/
Rendez-vous dans le dossier pipes/ : `cd pipes`

Exécutez la commande `make deepclean` : elle permet de nettoyer votre projet pour repartir sur des bases saines.

Exécutez la commande `make` pour compiler le projet : à la fois l'application 'client' et 'server'.

Normalement, votre dossier build/ contient désormais deux exécutables : 'server' et 'client' :
```
├── pipes/
│   ├── build/
│       ├── client
│       └── server
```

**2. Exécuter le projet**
Pour exécuter le projet, il vous suffit de lancer les exécutables (**1 par terminal !!**) :
- Lancer en premier le server : depuis le dossier pipes/ : `./build/server`
- Lancer ensuite un ou plusieurs clients : depuis le dossier pipes/ : `./build/client`

**3. Résolution des erreurs**
Si vous avez des erreurs, exécutez la commande `make deepclean` : elle permet de nettoyer votre projet pour repartir sur des bases saines.

## Utilisation du livrable PIPES
Après avoir lancé les exécutables 'server' et 'clients' (un ou plusieurs) dans des terminaux distincts, les clients se sont connectés automatiquement au server.

**1. Utilisation normale**
Vous pouvez envoyer un message quelconque depuis un terminal client. Le server répondra avec une réponse automatique.

**2. Déconnexion du client**
Il est possible de déconnecter le client du server 'proprement' en lui envoyant le message `CLOSE`. Le server prendra note de la déconnexion du client et fera le nécessaire pour fermer la connexion de son côté.

**3. Fermeture du client**
Il est possible de quitter le client avec la commande `CTRL + C` dans le terminal client. Le server prendra note de la déconnexion du client et fera le nécessaire pour fermer la connexion de son côté.

**4. Fermeture du server**
Il est possible de fermer le server avec la commande `CTRL + C` dans le terminal server. Le server fermera l'intégralité des connexions (=>déconnexion des clients) avant de s'éteindre.

## Compilation du livrable SOCKETS

**1. Build du projet**
Ouvrez un terminal à la racine du projet : chatRZO/
Rendez-vous dans le dossier sockets/ : `cd sockets`

Exécutez la commande `make deepclean` : elle permet de nettoyer votre projet pour repartir sur des bases saines.

Exécutez la commande `make` pour compiler le projet : à la fois l'application 'client' et 'server'.

Normalement, votre dossier build/ contient désormais deux exécutables : 'server' et 'client' :
```
├── sockets/
│   ├── build/
│       ├── client
│       └── server
```

**2. Exécuter le projet**
Pour exécuter le projet, il vous suffit de lancer les exécutables (**1 par terminal !!**) :
- Lancer en premier le server : depuis le dossier pipes/ : `./build/server`
- Lancer ensuite un ou plusieurs clients : depuis le dossier pipes/ : `./build/client`

**3. Résolution des erreurs**
Si vous avez des erreurs, exécutez la commande `make deepclean` : elle permet de nettoyer votre projet pour repartir sur des bases saines.

## Utilisation du livrable SOCKETS
Après avoir lancé les exécutables 'server' et 'clients' (un ou plusieurs) dans des terminaux distincts, les clients sont prêts à se connecter au server. Pour ce faire, vous devez renseigner le pseudo qui sera utilisé de manière publique sur le server.

**1. Utilisation normale**
Vous pouvez envoyer un message quelconque depuis un terminal client. Le server redirigera votre message vers tous les clients connectés qui verront ce que vous avez écris.

**2. Utilisation des commandes spéciales**
Il est possible d'utiliser certaines commandes prédéfinies sur le server :
- `/mp <PSEUDO> <MESSAGE>` : en remplaçant `<PSEUDO>` par le nom d'un client actuellement connecté au server et  `<MESSAGE>` par le message souhaiter, vous pouvez faire parvenir à l'utilisateur ciblé un message qui sera privé : visible par lui seul et totalement indétectable par les autres clients connectés au server.
- `/rename <PSEUDO>` : en remplaçant `<PSEUDO>` par un nom quelconque, vous pouvez changer votre nom d'utilisateur sur le server. Tous les autres clients connectés seront notifiés de ce changement.

**3. Fermeture du client**
Il est possible de quitter le client avec la commande `CTRL + C` dans le terminal client. Le server prendra note de la déconnexion du client et fera le nécessaire pour fermer la connexion de son côté.

**4. Fermeture du server**
Il est possible de fermer le server avec la commande `CTRL + C` dans le terminal server. Le server fermera l'intégralité des connexions (=>déconnexion des clients) avant de s'éteindre.