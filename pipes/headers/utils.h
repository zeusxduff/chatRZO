void input_to_message(char* arr, int length);
void clear_stdout();

typedef struct connection_server{
	int id; // Id unique permettant d'identifier la connexion
	int descPipeClientToServer; // Le descripteur du pipe client->server
	int descPipeServerToClient; // Le descripteur du pipe server->client
	pthread_t client_handler; // Le thread qui gère la réception et l'interprétation des messages du client
	char pipeClientToServer[SIZE_PIPE_NAME]; // le nom du pipe client->server
	char pipeServerToClient[SIZE_PIPE_NAME]; // Le nom du pipe server->client
	int flag; // Le falg de la connexion : 0 = en attente de connexion, 1 = connecté, 2 = fermé
	
	struct connection_server* next; // Le pointeur vers le prochain maillon de la liste chainée de connexions
}connection_server;
