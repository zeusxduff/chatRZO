#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>

#include "../headers/const.h"
#include "../headers/utils.h"
#include "../headers/server.h"

int nb_connections = 0; // Compte le nombre de connexions afin dé générer un id unique pour chaque client
connection_server* connections = NULL; // Tête de liste de la listé chainée répertoriant les connexions
volatile int flag = 0; // Flag qui permet d'interrompre le server 0=>server doit tourner, 1=>server doit s'arrêter

/**
 * Fonction permettant de définir le comportement à appliquer lors d'un ctrl C
 * */
void catch_ctrl_c_server()
{
    flag = 1; // On passe le flag du server à 1 pour ordonner l'arrêt
}

/**
 * Fonction permettant d'ajouter une connexion à la fin de la liste chainé des connexions
 * @param connection La connexion à ajouter à la liste
 * */
void add_connection(connection_server* connection)
{
	// Si la liste est vide, la connexion deient la tête de la liste
	if(connections == NULL)
	{
		connections = connection;
		return;
	}
	
	// Si la liste n'est pas vide, on ajoute la connexion à la fin de la liste
    connection_server* current = connections;
    while(current->next != NULL)
		current = current->next;
	current->next = connection;
	connection->next = NULL; // On oublie pas de forcer le prochain élément à NULL pour être sur d'avoir notre fin de liste
}

/**
 * Supprime une connexion de la liste, en fonction de l'id choisi
 * @param id l'identifien unique de la connexion à supprimer de la liste
 * */
void delete_connection(int id)
{
	// Cas 1 : la liste de connexions est vide
	if(connections == NULL)
	{
		//printf("Cas 1 : la liste de connexions est vide\n");
		return;
	}
	
	connection_server* tmp = NULL;
	
	// Cas 2 : la tête de la liste de connexions est à supprimer
	if(connections->id == id)
	{
		//printf("Cas 2 : la tête de la liste de connexions est à supprimer\n");
		tmp = connections->next; // On enlève la connection du maillage
		
		// On supprime la connexion
		pthread_cancel(connections->client_handler);
		
		if(connections->flag != 0) // Si la connection est établie avec un client
		{
			// On informe le client que la fermeture de la connexion a bien été prise en compte
			if(write(connections->descPipeServerToClient, "CLOSE", MSG_SIZE) == -1)
			{
				printf("Erreur lors de l'écriture du message CLOSE indiquant au client que la fermeture de la connexion a bien été prise en compte\n");
			}
		}
			
		// Fermeture du pipe de réception
		close(connections->descPipeClientToServer);
		
		// Fermeture du pipe d'envoi
		close(connections->descPipeServerToClient);
		
		// On supprime les pipes de l'ordinateur
		char command[MSG_SIZE];
		
		// Suppression du pipe de réception
		strcpy(command, "rm -rf ");
		strcat(command, connections->pipeClientToServer);
		if(system(command) == -1)
		{
			printf("Erreur lors de l'éxécution de la commande\n");
		}
		
		// Suppression du pipe d'envoi
		strcpy(command, "rm -rf ");
		strcat(command, connections->pipeServerToClient);
		if(system(command) == -1)
		{
			printf("Erreur lors de l'éxécution de la commande\n");
		}
		
		free(connections);
		connections = tmp;
		
		return;
	}
	
    connection_server* current = connections;
    while(current->next != NULL)
    {
		// Cas 3 : on trouve la connexion à supprimer dans la liste
		if(current->next->id == id) // Si on trouve la connection à supprimer
		{
			//printf("Cas 3 : on trouve la connexion à supprimer dans la liste\n");
			
			tmp = current->next; // On stocke temporairement la connection à supprimer
	
			current->next = current->next->next; // On enlève la connection du maillage
			
			// On supprime la connexion
			pthread_cancel(tmp->client_handler);
			
			if(connections->flag != 0) // Si la connection est établie avec un client
			{
				// On informe le client que la fermeture de la connexion a bien été prise en compte
				if (write(tmp->descPipeServerToClient, "CLOSE", MSG_SIZE) == -1)
				{
					printf("Erreur lors de l'écriture du message CLOSE indiquant au client que la fermeture de la connexion a bien été prise en compte\n");
				}
			}
				
			// Fermeture du pipe de réception
			close(tmp->descPipeClientToServer);
			
			// Fermeture du pipe d'envoi
			close(tmp->descPipeServerToClient);
			
			// On supprime les pipes de l'ordinateur
			char command[MSG_SIZE];
			
			// Suppression du pipe de réception
			strcpy(command, "rm -rf ");
			strcat(command, connections->pipeClientToServer);
			if(system(command) == -1)
			{
				printf("Erreur lors de l'éxécution de la commande\n");
			}
			
			// Suppression du pipe d'envoi
			strcpy(command, "rm -rf ");
			strcat(command, connections->pipeServerToClient);
			if(system(command) == -1)
			{
				printf("Erreur lors de l'éxécution de la commande\n");
			}
			
			// On supprime la connexion
			free(tmp);
			tmp = NULL;
			
			return;
		}
		
		current = current->next;
	}
	
	// Cas 4 : on ne trouve pas la connexion à supprimer dans la liste
	//printf("Cas 4 : on ne trouve pas la connexion à supprimer dans la liste\n");
}

/**
 * Ouvre une nouvelle connexion : ouvre les pipes d'écriture et de réception,
 * lance un thread pour gérer cette connexion avec le client en parallèle des autres clients,
 * met également à jour le fichier contenant les informations des connexions pour indiquer au prochain
 * client comment se connecter au server.
 * */
void open_connection()
{
	connection_server* connection = malloc(sizeof(connection_server)); // On alloue de l'espace pour contenir les informations de cette connexion
	connection->id = nb_connections; // On attribue un id unique à cette connexion
	connection->flag = 0; // On défini cette connexion comme en attente de connexion
	
	// On crée une chaine formée de l'id de la connexion pour l'utiliser à la création des pipes (qui seront donc unique pour cette connexion)
	char str_nb_connections[SIZE_PIPE_NAME];
	sprintf(str_nb_connections, "%d", nb_connections);
	
	// Génère le nom du pipeClientToServer
	strcpy(connection->pipeClientToServer, PIPE_CLIENT_TO_SERVER);
	strcat(connection->pipeClientToServer, str_nb_connections);
	
	// Génère le nom du pipreServerToClient
	strcpy(connection->pipeServerToClient, PIPE_SERVER_TO_CLIENT);
	strcat(connection->pipeServerToClient, str_nb_connections);
	
	// Ecriture des inforamtions concernant les pipes disponibles dans les fichiers
	FILE* file = fopen(CONNECTIONS_INFOS_FILE, "w+");
	if(file == NULL)
	{
		printf("Erreur lors de l'ouverture du fichier des informations des pipes\n");
		exit(0);
	}
	
	fprintf(file, "%d\n", nb_connections);
	fclose(file);
    
    nb_connections++; // On incrémente la variable du nombre de connexions, car une connexion vient d'être rajoutée. De plus celà garanti l'unicité des id de connexion
    
    // On ajoute la connexion à la liste des connexions
    add_connection(connection);
    
    // Réception de messages
    if (pthread_create(&(connection->client_handler), NULL, (void *) client_handler, NULL) != 0) {
        printf ("Erreur lors de la création du thread (réception des messages) !\n");
        exit(EXIT_FAILURE);
    }
}

/**
 * Fonction qui est exécutée au démarrage d'un thread de gestion de client.
 * A pour but de gérer une unique connexion (une connexion et un client par thread) entre le server et le client.
 * Ecoute les messages reçus et renvoi une réponse automatique
 * Il est actuellement impossible d'envoyer un message au client, hormis en réponse à un de ces messages (car inutile pour notre usage actuel,
 * pour ce faire il faudrait créer un nouveau thread pour la gestion de l'envoi des messages).
 * */
void client_handler()
{	
	// On récupère les informations de la connexion qui vient d'être créée
	connection_server* connection;
	connection_server* current = connections;
    while(current->next != NULL)
		current = current->next;
	connection = current;
	
	
	// Creation du pipe de réception : client->server
	if(mkfifo(connection->pipeClientToServer, 0666) != 0) // On crée le pipe
	{
	  perror("Problème de création du pipe client->server\n");
	  exit(1);
	}
	
	// Création du pipe d'envoi : server->client
	if(mkfifo(connection->pipeServerToClient, 0600) != 0) // On crée le pipe
	{
	  perror("Problème de création du pipe");
	  exit(1);
	}
	
	// Ouverture du pipe de réception : client->server
	connection->descPipeClientToServer = open(connection->pipeClientToServer, O_RDONLY); // On ouvre le pipe en lecture
	if(connection->descPipeClientToServer < 0)
	{
		printf("Erreur d'ouverture du pipe server->client\n");
	}
	
	// Ouverture du pipe d'envoi : server->client
	connection->descPipeServerToClient = open(connection->pipeServerToClient, O_WRONLY); // On ouvre le pipe en écriture
	if(connection->descPipeClientToServer < 0)
	{
		printf("Erreur d'ouverture du pipe\n");
	}
	
	while(1) // Boucle infini pour pouvoir recevoir en continu
	{
		char message[MSG_SIZE];
		int nb = 0;
		while(nb == 0) // On lit jusqu'à ce qu'on récupère un message cohérant
		{
			nb = read(connection->descPipeClientToServer, message, MSG_SIZE);
		}
		message[nb] = '\0'; // On ajoute le caractère de fin de chaîne pour former une chaine
		
		if(strcmp(message, "OPEN") == 0) // Interprétation du message OPEN : un client vient de se connecter à notre connexion
		{
			connection->flag = 1; // On passe le flag à 1 pour que le server sache que cette connexion est utilisée et qu'il doit en générer une nouvelle pour attendre un nouveau client
		}else if(strcmp(message, "CLOSE") == 0) // Interprétation du message CLOSE : le client connecté se déconnecte
		{
			connection->flag = 2; // On passe le flag à 2 pour indiquer au server la fin de connexion, ce dernier va pouvoir supprimer cette connexion et libérer la mémoire
		}else{ // Si le message ne correspond à rien d'interprétable, on effectue une réponse automatique
			// Log côté server
			clear_stdout();
			printf("<client %d> %s\n", connection->id, message);
			
			// Réponse automatique
			char msgBack[MSG_SIZE];
			strcpy(msgBack, "Réponse automatique du server : ");
			strcat(msgBack, message);
			if (write(connection->descPipeServerToClient, msgBack, MSG_SIZE) == -1)
			{
				printf("Erreur lors de l'écriture du message de réponse automatique du server\n");
			}
			
		}
	}
}

int main()
{
	printf("Lancement du serveur\n");
	
	if(system("mkdir -p /tmp/chatRZO") == -1)
	{
		printf("Erreur lors de l'éxécution de la commande\n");
	}
	
	signal(SIGINT, catch_ctrl_c_server); // On modifie le comportement par défaut lors d'un ctrl C
	
	open_connection(); // On ouvre une connexion : pour que le server soit prêt à accepter son premier client
    
    // On boucle à l'infini, tant que le flag est à 0 : s'il passe à 1, on doit fermer le server
    while(flag == 0)
    {
		// On parcours les connexions
		connection_server* current = connections;
		while(current->next != NULL)
		{
			if(current->flag == 2) // Si la connexion a été interrompue par le client
			{
				connection_server* tmp = current->next;
				delete_connection(current->id);
				current = tmp;
			}else{
				current = current->next;
			}			
		}
		
		if(current->flag == 1) // Si la nouvelle connection a été utilisée, on en génère une nouvelle
		{
			open_connection();
		}
    }
    
    // On ferme tous les threads
	connection_server* current = connections;
	if(connections != NULL)
	{
		while(current != NULL)
		{
			connection_server* tmp = current->next;
			delete_connection(current->id);
			current = tmp;
		}
	}
	
	// On supprime le fichier contenant les infos de connexion
	char command[MSG_SIZE];
	strcpy(command, "rm -rf ");
	strcat(command, CONNECTIONS_INFOS_FILE);
	if(system(command) == -1)
	{
		printf("Erreur lors de l'éxécution de la commande\n");
	}
		
	printf("\rFermeture du serveur\n");
	return 0;
}

