#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>

#include "../headers/const.h"
#include "../headers/utils.h"
#include "../headers/client.h"

volatile int flag = 0; // Flag qui permet d'interrompre le client : 0 => client doit trouner, 1 => client doit s'arrêter
volatile int descPipeServerToClient; // Descripteur du pipe server->client
volatile int descPipeClientToServer; // Descripteur du pipe client->server
pthread_t recv_msg_thread; // Thread qui gère la réception des messages par le client
pthread_t send_msg_thread; // Thread qui gère l'envoi des messages par le client
char pipeClientToServer[SIZE_PIPE_NAME]; // Nom du pipe client->server
char pipeServerToClient[SIZE_PIPE_NAME]; // Nom du pipe server->client

/**
 * Fonction permettant de définir le comportement à appliquer lors d'un ctrl C
 * */
void catch_ctrl_c_client()
{
    if(write(descPipeClientToServer, "CLOSE", MSG_SIZE) == -1) // On envoie le message pour demander la fermeture au server
	{
		printf("Erreur lors de l'écriture du message saisi par le client en direction du serveur\n");
	}
}

/**
 * Ouvre une connexion : récupère les informations dans le fichier prévu à cet effet et suit les indications
 * pour ouvrir les pipes. Une fois connecté, envoi un message OPEN au server pour le prévenir que la connexion proposée
 * est désormais occupée
 * */
int open_connection()
{
	int nb_connections = 0; // Pour stocker le nombre de connexions du server / l'identifiant unique de la connexion qui sera utilisée
	
	// Lecture des informations concernant les pipes disponibles dans le fichier
	FILE* file = fopen(CONNECTIONS_INFOS_FILE, "r");
	if(file == NULL)
	{
		printf("Le serveur n'est pas ouvert\n");
		return -1;
	}
	
	// On récupère dans le fichier le numéro de la connexion à utiliser pour se connecter
	if(fscanf(file, "%d\n", &nb_connections) == EOF)
	{
		printf("Erreur lors de la lecture des informations de connexion\n");
		return -1;
	}
	fclose(file);
	
	// On transforme le numéro de connexion récupéré en chaine pour pouoir l'utiliser
	char str_nb_connections[SIZE_PIPE_NAME];
	sprintf(str_nb_connections, "%d", nb_connections);
	
	// Génère le nom du pipeClientToServer
	strcpy(pipeClientToServer, PIPE_CLIENT_TO_SERVER);
	strcat(pipeClientToServer, str_nb_connections);
	
	// Génère le nom du pipreServerToClient
	strcpy(pipeServerToClient, PIPE_SERVER_TO_CLIENT);
	strcat(pipeServerToClient, str_nb_connections);
	
	// Réception de messages
    if (pthread_create(&recv_msg_thread, NULL, (void *) recv_msg_handler, NULL) != 0) {
        printf ("Erreur lors de la création du thread (réception des messages) !\n");
        exit(EXIT_FAILURE);
    }
	
	// Envoi des messages
    if (pthread_create(&send_msg_thread, NULL, (void *) send_msg_handler, NULL) != 0) {
        printf ("Erreur lors de la création du thread (envoi des messages) !\n");
        exit(EXIT_FAILURE);
    }
    
    return 0;
}

/**
 * Fonction qui est exécutée au démarrage d'un thread de réception de messages.
 * */
void recv_msg_handler()
{
	// Ouverture du pipe de réception : server->client
	descPipeServerToClient = open(pipeServerToClient, O_RDONLY); // On ouvre le pipe en lecture
	
	if(descPipeServerToClient < 0) // Si le pipe n'existe pas
	{
		printf("Erreur lors de l'ouverture du pipe\n");
		exit(0);
	}
		
	while(1) // Boucle infini pour pouvoir recevoir en continu
	{
		char message[MSG_SIZE]; // Stocke le message reçu
		int nb = 0; // Stocke la taille du message reçu
		while(nb == 0) // On lit jusqu'à recevoir un message cohérent
		{
			nb = read(descPipeServerToClient, message, MSG_SIZE);
		}
		message[nb] = '\0'; // On ajoute le caractère de fin de chaine au message reçu
		
		if(strcmp(message, "CLOSE") == 0) // Interprétation du message CLOSE : le client doit se fermer
		{
			flag = 1; // On passe le falg à 1 pour indiquer que le client doit être fermé
		}else{ // Si le message ne correspond à aucune interprétation, on l'affiche simplement
			clear_stdout();
			printf("<server> %s\n", message);
		}
	}
}

/**
 * Fonction qui est exécutée au démarrage d'un thread d'envoi de messages
 * */
void send_msg_handler()
{
	// Ouverture du pipe d'envoi : client->server
	descPipeClientToServer = open(pipeClientToServer, O_WRONLY); // On ouvre le pipe en écriture
	if(descPipeClientToServer < 0) // Si le pipe n'existe pas encore
	{
		printf("Erreur lors de l'ouverture du pipe\n");
		exit(0);
	}
	
	// Préviens le server qu'on s'est connecté
	if (write(descPipeClientToServer, "OPEN", MSG_SIZE) == -1)
	{
		printf("Erreur lors de l'écriture du message OPEN indiquant au server que le client est connecté\n");
	}
	
	while(1) // Boucle infinie pour envoyer en continu
	{
		char message[MSG_SIZE]; // Stocke le message à envoyer
		if(fgets(message, MSG_SIZE, stdin) == NULL) // on récupère la saisie de l'utilisateur
		{
			printf("Erreur lors de la lecture du message reçu du server\n");
		}
		input_to_message(message, MSG_SIZE); // On transforme la saisie en message utilisable
		
		if(strlen(message) > 0) // On vérifie la cohérence du message
		{
			if(write(descPipeClientToServer, message, MSG_SIZE) == -1) // On envoie le message
			{
				printf("Erreur lors de l'écriture du message saisi par le client en direction du serveur\n");
			}
		}
	}
}

int main()
{
	printf("Lancement du client\n");
	
	signal(SIGINT, catch_ctrl_c_client); // On modifie le comportement par défaut lors d'un ctrl C
	
	if(open_connection() != -1) // On se connecte au server
	{
		while(flag == 0) // On boucle à l'infini tant que le client ne se ferme pas
		{
			
		}
		
		// Fermeture des threads
		pthread_cancel(recv_msg_thread);
		pthread_cancel(send_msg_thread);
		
		// Fermeture du pipe de réception
		close(descPipeServerToClient);
		
		// Fermeture du pipe d'envoi
		close(descPipeClientToServer);
	}else{
		printf("Echec de la connexion au server\n");
	}
	
	printf("\rFermeture du client\n");
	return 0;
}

