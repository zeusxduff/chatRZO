#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../headers/const.h"
#include "../headers/utils.h"

/**
 * Modifie une chaine récupérée avec fgets sur stdin en message (chaine utilisable)
 * @param arr le tableau de char récupéré depuis fgets
 * @param length la taille du tableau de char récupéré depuis fgets
 * */
void input_to_message(char* arr, int length)
{
	for (int i = 0; i < length; i++) { // On parcours le tableau de char en entier (ou pas selon si on break avant la fin)
        if (arr[i] == '\n') { // Dès que l'on croise un \n, la commande se termine
            arr[i] = '\0'; // On indique la fin de la chaine grâce à \0
            break; // On quitte la boucle for, inutile de continuer
        }
    }
}

/**
 * Prépare le terminal à un affichage afin que celui-ci reste propre
 * */
void clear_stdout()
{
    printf("\r"); // On supprime ce qu'il y a sur la ligne d'affichage
    fflush(stdout); // On vide la mémoire tampon liée à l'affichage afin de pouvoir accueillir du nouveau texte sans déchets résiduels
}
