void input_to_string(char* arr, int length);
void clear_stdout();
int start_with(char *prefix, char *string);
void get_receiver_name(char buffer[MESSAGE_SIZE], char receiver[NAME_SIZE]);

/**
 * Structure servant de maillon à la liste chainée qui nous sert à garder en mémoire les informations concernant les clients connectés au serveur
 * */
typedef struct connection_server {
    int id; // Le numéro de la socket
    char ip[16]; // L'ip du client (16 bits suffisent normalement)
    int flag;
    pthread_t client_handler; // Le thread qui gère la réception et l'interprétation des messages du client
    char nickname[NAME_SIZE];
    
    struct connection_server* next; // Le maillon suivant
} connection_server;
