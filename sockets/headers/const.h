#define MESSAGE_SIZE 1200 // La taille max d'un message
#define NAME_SIZE 200 // La taille max d'un pseudo
#define PORT 4444 // Le numéro du port à utiliser
#define CREDITS_ONE "Voici les crédits (prénoms des développeurs de chatRZO :\n\n\
oooooo   oooo                                   \n\
 `888.   .8\'                                    \n\
  `888. .8\'    .oooo.   ooo. .oo.   ooo. .oo.   \n\
   `888.8\'    `P  )88b  `888P\"Y88b  `888P\"Y88b  \n\
    `888\'      .oP\"888   888   888   888   888  \n\
     888      d8(  888   888   888   888   888  \n\
    o888o     `Y888\"\"8o o888o o888o o888o o888o \n"
#define CREDITS_TWO "\
\
   oooo                                                 o8o            \n\
   `888                                                 `\"\'            \n\
    888  .ooooo.  oooo d8b  .ooooo.  ooo. .oo.  .oo.   oooo   .ooooo.  \n\
    888 d88\' `88b `888\"\"8P d88\' `88b `888P\"Y88bP\"Y88b  `888  d88\' `88b \n\
    888 888ooo888  888     888ooo888  888   888   888   888  888ooo888 \n\
    888 888    .o  888     888    .o  888   888   888   888  888    .o \n\
.o. 88P `Y8bod8P\' d888b    `Y8bod8P\' o888o o888o o888o o888o `Y8bod8P\' \n\
`Y888P    \n"
#define CREDITS_THREE "\
ooooo      ooo               .   oooo                              \n\
`888b.     `8\'             .o8   `888                              \n\
 8 `88b.    8   .oooo.   .o888oo  888 .oo.    .oooo.   ooo. .oo.   \n\
 8   `88b.  8  `P  )88b    888    888P\"Y88b  `P  )88b  `888P\"Y88b  \n\
 8     `88b.8   .oP\"888    888    888   888   .oP\"888   888   888  \n\
 8       `888  d8(  888    888 .  888   888  d8(  888   888   888  \n\
o8o        `8  `Y888\"\"8o   \"888\" o888o o888o `Y888\"\"8o o888o o888o \n"
#define CREDITS_FOUR "\
ooooooooooooo oooo                                                       \n\
8'   888   `8 `888                                                       \n\
     888       888 .oo.    .ooooo.  ooo. .oo.  .oo.    .oooo.    .oooo.o \n\
     888       888P\"Y88b  d88' `88b `888P\"Y88bP\"Y88b  `P  )88b  d88(  \"8 \n\
     888       888   888  888   888  888   888   888   .oP\"888  `\"Y88b.  \n\
     888       888   888  888   888  888   888   888  d8(  888  o.  )88b \n\
    o888o     o888o o888o `Y8bod8P\' o888o o888o o888o `Y888\"\"8o 8\"\"888P\' \n"
#define HELP "Voici la listes des commandes disponibles :\n\
/rename <PSEUDO>\n\
/mp <PSEUDO> <MESSAGE>\n\
/credits"
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define BLACK "\e[30m"
#define RESET "\x1B[0m"
#define GREY "\e[37m"
