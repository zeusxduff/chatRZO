void catch_ctrl_c_server();
void send_to_all_clients(connection_server* sender, char* tmp_message);
void send_message_disconnect(char nickname[NAME_SIZE]);
void send_message_server_closed();
void send_message_rename(char old_name[NAME_SIZE], char new_name[NAME_SIZE]);
void send_message_connection(connection_server* connection);
void delete_connection(int id);
void add_connection(connection_server* connection);
connection_server* init_connection(int sockfd, char* ip);
void client_handler(void* connection_param);
int command_handler(connection_server* connection, char recv_buffer[MESSAGE_SIZE]);
void send_message(connection_server* sender, char tmp_message[MESSAGE_SIZE], char receiver[NAME_SIZE]);
int main(void);
