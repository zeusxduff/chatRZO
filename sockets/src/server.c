#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h> // Pour pouvoir utiliser les threads (pour le multi-clients)
#include <ctype.h>

#include "../headers/const.h"
#include "../headers/utils.h"
#include "../headers/server.h"

connection_server* connections = NULL; // Tête de liste de la listé chainée répertoriant les connexions
int server_sockfd = 0;

/**
 * Fonction permettant de définir le comportement à appliquer lors d'un ctrl C
 * */
void catch_ctrl_c_server()
{
	send_message_server_closed();
	
	while(connections != NULL)
	{
		connection_server* tmp = connections;
		connections = connections->next;
		
		close(tmp->id); // On ferme la socket du client
		pthread_cancel(tmp->client_handler); // On ferme le thread
		free(tmp); // On libère la mémoire
		tmp = NULL;
	}
	
    close(server_sockfd); // On ferme la socket du server
    
    printf("\rFermeture du serveur\n");
    exit(0); // On quitte le programme
}

/**
 * Fonction permettant d'envoyer un message à tous les clients excepté l'envoyeur
 * @param sender l'adresse de la connexion qui envoi le message
 * @param tmp_message le message envoyé
 * */
void send_to_all_clients(connection_server* sender, char tmp_message[MESSAGE_SIZE])
{
    char message[MESSAGE_SIZE] = {};
    sprintf(message, "<%s> %s", sender->nickname, tmp_message); // On prépare le message avant de l'envoyer en précisant qui l'a envoyé

	connection_server* tmp = connections;
    while (tmp != NULL) { // Tant que tmp != NULL <=> Tant qu'il y a des maillons dans la liste chainée
        if (sender->id != tmp->id) { // On envoi le message à tous les clients sauf celui qui a envoyé le message
            send(tmp->id, message, MESSAGE_SIZE, 0); // On envoi le message au client
        }
        tmp = tmp->next; // On passe au client suivant
    }
}

/**
 * Prévient tous les utilisateurs d'une déconnexion
 * @param nickname le pseudo de l'utilisateur qui se déconnecte
 * */
void send_message_disconnect(char nickname[NAME_SIZE])
{
	char message[MESSAGE_SIZE] = {};
    sprintf(message, GRN"%s a quitté le server"RESET, nickname); // On prépare le message avant de l'envoyer

	connection_server* tmp = connections;
    while (tmp != NULL) { // Tant que tmp != NULL <=> Tant qu'il y a des maillons dans la liste chainée
        send(tmp->id, message, MESSAGE_SIZE, 0); // On envoi le message au client
        tmp = tmp->next; // On passe au client suivant
    }
}

/**
 * Prévient tous les utilisateurs que le server ferme
 * */
void send_message_server_closed()
{
	char message[MESSAGE_SIZE] = GRN"Le server a fermé"RESET;

	connection_server* tmp = connections;
    while (tmp != NULL) { // Tant que tmp != NULL <=> Tant qu'il y a des maillons dans la liste chainée
        send(tmp->id, message, MESSAGE_SIZE, 0); // On envoi le message au client
        tmp = tmp->next; // On passe au client suivant
    }
}

/**
 * Prévient tous les utilisateurs qu'un utilisateur s'est renommé
 * @param old_name l'ancien nom de l'utilisateur
 * @param new_name le nouveau nom de l'utilisateur
 * */
void send_message_rename(char old_name[NAME_SIZE], char new_name[NAME_SIZE])
{
	char message[MESSAGE_SIZE] = {};
    sprintf(message, GRN"%s a changé son pseudo pour %s"RESET, old_name, new_name); // On prépare le message avant de l'envoyer

	connection_server* tmp = connections;
    while (tmp != NULL) { // Tant que tmp != NULL <=> Tant qu'il y a des maillons dans la liste chainée
        send(tmp->id, message, MESSAGE_SIZE, 0); // On envoi le message au client
        tmp = tmp->next; // On passe au client suivant
    }
}

/**
 * Prévient tous les utilisateurs qu'un nouvel utilisateur est connecté
 * @param connection la nouvelle connexion
 * */
void send_message_connection(connection_server* connection)
{
	char message[MESSAGE_SIZE] = {};
    sprintf(message, GRN"%s a rejoint le server"RESET, connection->nickname); // On prépare le message avant de l'envoyer

	connection_server* tmp = connections;
    while (tmp != NULL) { // Tant que tmp != NULL <=> Tant qu'il y a des maillons dans la liste chainée
		if(tmp->id != connection->id)
		{
			send(tmp->id, message, MESSAGE_SIZE, 0); // On envoi le message au client
		}
		tmp = tmp->next; // On passe au client suivant
    }
}

/**
 * Supprime une connexion de la liste des connexions
 * @param id l'id de la connexion à supprimer
 * */
void delete_connection(int id)
{
	int flag = 0; // Pour analyser le résultat
	
	// Si la liste est vide, il n'y a rien à supprimer
	if(connections == NULL)
	{
		
	}else if(connections->id == id){ // Si la connexion à supprimer est la tête de liste
		connection_server* tmp = connections;
		connections = connections->next;
		
		send_message_disconnect(tmp->nickname);
		
		close(tmp->id); // On ferme la socket du client
		pthread_cancel(tmp->client_handler); // On ferme le thread
		free(tmp); // On libère la mémoire
		tmp = NULL;
		
		flag = 1;
	}else{
		connection_server* connection = connections;
		while(connection->next != NULL)
		{
			if(connection->next->id == id)
			{
				connection_server* tmp = connection->next;
				connection->next = tmp->next;
				
				send_message_disconnect(tmp->nickname);
				
				close(tmp->id); // On ferme la socket du client
				pthread_cancel(tmp->client_handler); // On ferme le thread
				free(tmp); // On libère la mémoire
				tmp = NULL;
				flag = 1; // Connexion trouvée et supprimée
				break;
			}
			connection = connection->next;
		}
	}
	
	
	if(!flag)
	{
		printf("La connexion a supprimer n'a pas été trouvée\n");
	}
}

/**
 * Fonction permettant d'ajouter une connexion à la fin de la liste chainé des connexions
 * @param connection La connexion à ajouter à la liste
 * */
void add_connection(connection_server* connection)
{
	// Si la liste est vide, la connexion deient la tête de la liste
	if(connections == NULL)
	{
		connections = connection;
		return;
	}
	
	// Si la liste n'est pas vide, on ajoute la connexion à la fin de la liste
    connection_server* current = connections;
    while(current->next != NULL)
		current = current->next;
	current->next = connection;
	connection->next = NULL; // On oublie pas de forcer le prochain élément à NULL pour être sur d'avoir notre fin de liste
}

/**
 * Initialise la connexion côté server
 * @param sockfd qui décrit la connexion entrente
 * @param ip l'ip de la connexion
 * */
connection_server* init_connection(int sockfd, char* ip)
{
	connection_server* connection = (connection_server *)malloc(sizeof(connection_server)); // On alloue dynamiquement la mémoire permettant de stocker le maillon
	
    connection->id = sockfd; // Le numéro de la socket
    strcpy(connection->ip, ip); // On copie l'adresse IPV4 récupérée précédemment (16 bits suffisent normalement)
    connection->flag = 0;
    
    return connection; // on renvoit la structure créee et initialisée, prête à l'usage
}

/**
 * Fonction qui s'occupe de gérer les interactions entre le server et le client
 * @param connection_param les informations de la connexion au client connection_param
 * */
void client_handler(void* connection_param)
{	
	connection_server* connection = (connection_server*) connection_param; // On récupère la connexion
	
	char recv_buffer[MESSAGE_SIZE] = {}; // On défini la variable qui servira à stocker le message reçu
    char send_buffer[MESSAGE_SIZE] = {}; // On défini la variable qui servira à stocker le message envoyé
    
    if(recv(connection->id, connection->nickname, NAME_SIZE, 0) <= 0) // On récupère le pseudo entré par l'utilisateur
    {
		printf("Une erreur est survenue\n");
	}
    
    // On affiche la connexion dans les logs
    printf("%s vient de se connecter\n", connection->nickname);
    
    // On envoi le message de bienvenue maintenant qu'on a récupérer le nom du client
	sprintf(send_buffer, "Bienvenue %s sur le serveur !", connection->nickname);
	send(connection->id, send_buffer, MESSAGE_SIZE, 0); // On envoi le message au client
	send_message_connection(connection);
	
	while (1) {
		int receive = recv(connection->id, recv_buffer, MESSAGE_SIZE, 0); // FONCTION BLOCANTE> On récupère le message reçu -> renvois -1 si erreur, 0 si la connexion est coupée, n la taille du message sinon
		
		 if (receive > 0) { // Si le message a été reçu correctement
			 
			 if(!command_handler(connection, recv_buffer))
			 {
				printf("<%s> %s\n", connection->nickname, recv_buffer); // On affiche le message dans les logs
				send_to_all_clients(connection, recv_buffer);
			 }
		 }else if (receive == 0) { // Si le client a été déconnecté pour une raison quelconque
			 printf("%s a quitté le server\n", connection->nickname); // On affiche le message dans les logs
			 delete_connection(connection->id);
		 }else { // Un erreur est survenue
			 perror("Une erreur est survenue dans la réception du message\n");
		 }
	}
}

/**
 * Gère les commandes : vérifie si le message est une commande et l'interprète si nécessaire
 * @param connextion la connexion qui a envoyé le message
 * @param recv_buffer le message reçu
 * */
int command_handler(connection_server* connection, char recv_buffer[MESSAGE_SIZE])
{
	if(start_with("/rename ", recv_buffer))
	{
		char tmp_name[NAME_SIZE]; // Nom temporaire pour la manip
		strncpy(tmp_name,recv_buffer+8, NAME_SIZE); // Technique pour substring en C
		printf("%s a changé son pseudo pour %s\n", connection->nickname, tmp_name); // On affiche le message dans les logs
		send_message_rename(connection->nickname, tmp_name);
		strcpy(connection->nickname, tmp_name);  // Mise à jour du nom
		
		return 1;
	}
	
	if(start_with("/mp ", recv_buffer))
	{
		char buffer[MESSAGE_SIZE] = {};
		strcpy(buffer, recv_buffer);
		char receiver[NAME_SIZE];
		
		strncpy(buffer, buffer + strlen("/mp") + 1, MESSAGE_SIZE); // On supprime le /mp
		get_receiver_name(buffer, receiver); // On récupère le nom
		strncpy(buffer, buffer + strlen(receiver) + 1, MESSAGE_SIZE); // On supprime le nom
		
		send_message(connection, buffer, receiver);
		
		return 1;
	}
	
	if(strcmp("/credits", recv_buffer) == 0)
	{
		send(connection->id, CREDITS_ONE, MESSAGE_SIZE, 0); // On envoi le message au client : partie 1
		send(connection->id, CREDITS_TWO, MESSAGE_SIZE, 0); // On envoi le message au client : partie 2
		send(connection->id, CREDITS_THREE, MESSAGE_SIZE, 0); // On envoi le message au client : partie 3
		send(connection->id, CREDITS_FOUR, MESSAGE_SIZE, 0); // On envoi le message au client : partie 4
	}
	
	if(strcmp("/help", recv_buffer) == 0)
	{
		send(connection->id, HELP, MESSAGE_SIZE, 0); // On envoi le message au client : partie 1
	}
	
	return 0;
}

/**
 * Envoi un message à un client
 * @param sender la connexion qui envoi le message
 * @param tmp_message le message a envoyer
 * @param receiver le nom du destinataire du message
 * */
void send_message(connection_server* sender, char tmp_message[MESSAGE_SIZE], char receiver[NAME_SIZE])
{
    char message[MESSAGE_SIZE] = {};
       
    sprintf(message, RED"<%s>-><Vous> %s"RESET, sender->nickname, tmp_message); // On prépare le message avant de l'envoyer

    connection_server* tmp = connections;
    while(tmp != NULL) { // Tant que tmp != NULL <=> Tant qu'il y a des maillons dans la liste chainée
        if(sender->id != tmp->id && strcmp(receiver, tmp->nickname) == 0) { // On envoi le message seulement au destinataire du message privé
			printf("<%s>-><%s> %s\n", sender->nickname, receiver, tmp_message);
            send(tmp->id, message, MESSAGE_SIZE, 0); // On envoi le message au client
        }
        tmp = tmp->next; // On passe au client suivant
    }
}

int main(void)
{
	printf("Lancement du serveur\n");
	
	signal(SIGINT, catch_ctrl_c_server); // On modifie le comportement par défaut lors d'un ctrl C
	
	// Création de la socket
    server_sockfd = socket(AF_INET , SOCK_STREAM , 0); // Ouverture SOCK_STREAM = Mode TCP : bidirectionnel, message arrive à coup sur, messages arrivent dans l'ordre
    if (server_sockfd == -1) { // Si la socket = -1, il y a eu une erreur.
        printf("Erreur lors de la création de la socket serveur.");
        return 0; // On quitte le programme
    }
    
    // Informations de la socket
    struct sockaddr_in server_info, client_info; // Pour stocker les adresses internet du client et du server
    int s_addrlen = sizeof(server_info); // La taille de l'adresse server
    int c_addrlen = sizeof(client_info); // La taille de l'adresse client
    memset(&server_info, 0, s_addrlen); // On initialise la mémoire de l'adresse internet du server avec des 0
    memset(&client_info, 0, c_addrlen); // On initialise la mémoire de l'adresse internet du client avec des 0
    server_info.sin_family = PF_INET; // PF_INET : Protocol Family Internet : défini notre adresse comme une adresse internet
    server_info.sin_addr.s_addr = INADDR_ANY; // Pour que le server accepte les connexions de toutes les IP de cette machine
    server_info.sin_port = htons(PORT); // On défini le port de la connexion (htons converti du stockage octet vers le stockage réseau)

    // D'après les recherches sur internet, l'utilisation de SO_REUSEADDR devrait permettre de libérer plus rapidemment le port après fermeture du serveur.
    // A VERIFIER SUR LE LONG TERME, pour le moment ça à l'air de marcher
    if (setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0){ // On manipule lees options de socket, SOL_SOCKET = on utilise la couche socket
		printf("L'utilisation de SO_REUSEADDR a échouée ! ");
	}
	
	// Bind et Listen
    bind(server_sockfd, (struct sockaddr *)&server_info, s_addrlen); // Relie la socket à une adresse et un port
    listen(server_sockfd, 3); // Met la socket en attente de connexion
    
    // Affiche les informations du serveur
    getsockname(server_sockfd, (struct sockaddr*) &server_info, (socklen_t*) &s_addrlen); // On récupère les informations de la socket server générée
    //printf("Informations du serveur: %s:%d\n", inet_ntoa(server_info.sin_addr), ntohs(server_info.sin_port));
	
	while(1) // Boucle infinie pour attendre en continu des connexions de clients
	{
		int client_sockfd = accept(server_sockfd, (struct sockaddr*) &client_info, (socklen_t*) &c_addrlen); // On accepte toutes les connexions
		
		// Récupère les informations sur le client qui vient de se connecter et les affiche
        getpeername(client_sockfd, (struct sockaddr*) &client_info, (socklen_t*) &c_addrlen);
        //printf("%s:%d est connecté.\n", inet_ntoa(client_info.sin_addr), ntohs(client_info.sin_port));
        
        connection_server* connection = init_connection(client_sockfd, inet_ntoa(client_info.sin_addr)); // On initialise la connexion à partir des infos de la connexion (sock_fd) et de l'adresse ip (inet_ntoa(client_info.sin_addr))
        add_connection(connection);
        
        if (pthread_create(&(connection->client_handler), NULL, (void*)client_handler, (void*)connection) != 0) { // Si la création de thread renvoi autre chose que 0, il y a une erreur
            perror("Erreur lors de la création du thread !\n");
        }
	}
	
	return 0;
}
