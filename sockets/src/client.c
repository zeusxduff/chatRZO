#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h> // Pour pouvoir utiliser les threads (pour le multi-clients)
#include <ctype.h>

#include "../headers/const.h"
#include "../headers/utils.h"
#include "../headers/client.h"

volatile int flag = 0; // Flag qui permet d'interrompre le client : 0 => client doit trouner, 1 => client doit s'arrêter
int client_sockfd = 0; // Socket du client
char nickname[NAME_SIZE] = {}; // Pseudo du client

/**
 * Fonction permettant de définir le comportement à appliquer lors d'un ctrl C
 * */
void catch_ctrl_c_client()
{
	flag = 1;
}

/**
 * Fonction permettant de recevoir des messages en continu (ouvert sur un thread)
 * Affiche le message reçu dans le terminal
 * */
void recv_msg_handler()
{
    char receiveMessage[MESSAGE_SIZE] = {}; // On initialise la variable qui stockera le message
    while(1){ // Boucle infini pour recevoir en continu
        int receive = recv(client_sockfd, receiveMessage, MESSAGE_SIZE, 0); // FONCTION BLOCANTE> On récupère le message reçu -> renvois -1 si erreur, 0 si la connexion est coupée, n la taille du message sinon
        if (receive > 0) { // Si receive > 0 -> un message a été reçu
            printf("\r%s\n", receiveMessage); // On affiche le message
            clear_stdout(); // Après avoir afficher le message on prépare la nouvelle ligne (prêt pour l'écriture d'une commande)
        } else if (receive == 0) { // si receive = 0, la connexion a été coupé -> on quitte
			flag = 1;
            break;
        } else { // Si receive = -1, il y a eu un problème dans la reception du message, on ne fais rien.

        }
    }
}

/**
 * Fonction permettant d'envoyer des messages en continu (ouvert sur un thread)
 * */
void send_msg_handler()
{
    char message[MESSAGE_SIZE] = {}; // On initialise la variable qui stockera le message
    while(1){ // Boucle infini pour pouvoir envoyer en continu
        clear_stdout(); // On prépare la ligne du terminal pour l'écriture
        while (fgets(message, MESSAGE_SIZE, stdin) != NULL) { // Tant que la commande saisie n'est pas vide
            input_to_string(message, MESSAGE_SIZE); // On ajout le caractère de fin de chaine au bon endroit
            if (strlen(message) == 0) { // Si le message est vide, on reprépare une ligne pour écrire
                clear_stdout();
            } else { // Sinon on sort de la boucle, le message est prêt pour l'envoi
                break;
            }
        }
        send(client_sockfd, message, MESSAGE_SIZE, 0); // On envoi le message
    }
}

int main(void)
{
	printf("Lancement du client\n"); // On modifie le comportement par défaut lors d'un ctrl C
	
	signal(SIGINT, catch_ctrl_c_client);
	
	printf("Entrez votre pseudo: ");
      // On récupère la saisi du client
      if (fgets(nickname, NAME_SIZE, stdin) != NULL) {
          input_to_string(nickname, NAME_SIZE);
      }
      if (strlen(nickname) > 0 && strlen(nickname) >= NAME_SIZE) { // On vérifie que le pseudo saiso est correct
          printf("\nLe pseudo saisi est incorrect\n");
      }
	
	// On crée la socket
    client_sockfd = socket(AF_INET , SOCK_STREAM , 0);
    if (client_sockfd == -1) {
        printf("Erreur lors de la création de la socket.");
        exit(EXIT_FAILURE);
    }
    
    // Informations de la socket
    struct sockaddr_in server_info, client_info; // Pour stocker les adresses internet du client et du server
    int s_addrlen = sizeof(server_info); // La taille de l'adresse server
    int c_addrlen = sizeof(client_info); // La taille de l'adresse client
    memset(&server_info, 0, s_addrlen); // On initialise la mémoire de l'adresse internet du server avec des 0
    memset(&client_info, 0, c_addrlen); // On initialise la mémoire de l'adresse internet du client avec des 0
    server_info.sin_family = PF_INET; // PF_INET : Protocol Family Internet : défini notre adresse comme une adresse internet
    server_info.sin_addr.s_addr = inet_addr("127.0.0.1"); // On indique l'adresse du server
    server_info.sin_port = htons(PORT); // On défini le port de la connexion (htons converti du stockage octet vers le stockage réseau)
    
    // Connexion au serveur
    int err = connect(client_sockfd, (struct sockaddr *)&server_info, s_addrlen); // On essaye de connecter notre client au server en TCP
    if (err == -1) {
        printf("Echec de la connexion au serveur !\n");
        exit(EXIT_FAILURE);
    }
    
    send(client_sockfd, nickname, NAME_SIZE, 0); // On envoi au serveur le pseudo entré par le client
    
    // On crée un thread pour s'occuper de l'envoi des messages en continu (en l'isolant sur un thread)
    pthread_t send_msg_thread;
    if (pthread_create(&send_msg_thread, NULL, (void *) send_msg_handler, NULL) != 0) {
        printf ("Erreur lors de la création du thread (envoi des messages) !\n");
        exit(EXIT_FAILURE);
    }

	// On crée un thread pour s'occuper de la réception des messages en continu (en l'isolant sur un thread)
    pthread_t recv_msg_thread;
    if (pthread_create(&recv_msg_thread, NULL, (void *) recv_msg_handler, NULL) != 0) {
        printf ("Erreur lors de la création du thread (réception des messages) !\n");
        exit(EXIT_FAILURE);
    }
	
	while(!flag)
	{
		
	}
	
	pthread_cancel(recv_msg_thread); // On ferme le thread de reception
	pthread_cancel(send_msg_thread); // On ferme le thread d'envoi
	close(client_sockfd); // On ferme la socket
	
	printf("\rFermeture du client\n");
	return 0;
}
